let btnSearch = document.querySelector('.nav__btn');
let inputSearch = document.querySelector('.search__input');

btnSearch.addEventListener('click', () => {
    if(inputSearch.classList.contains('default')){
        inputSearch.classList.remove('default');
        inputSearch.classList.add('active');
    } else {
        inputSearch.classList.remove('active');
        inputSearch.classList.add('default');
    } 
});